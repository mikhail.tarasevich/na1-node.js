const fs = require('fs').promises;
const http = require('http');
const port = 3097;
const contentTemplate1 = '<!DOCTYPE HTML><html><head><meta charset="utf-8"><title>3097</title></head><script>const errormsg = "###err###";if (errormsg!="") {alert(errormsg)}</script><body><form action="/parser" method="get"><p>Имя:<input type="text" name="str1" value="###1###"></p><p>Возраст:<input type="text" name="str2" value="###2###"></p><p><input type="submit" value="Отправить"></p></form></body></html>';
const contentTemplate2 = '<!DOCTYPE HTML><html><head><meta charset="utf-8"><title>3097</title></head><body><div><p>Имя:<output name="str1">###1###</output></p><p>Возраст:<output name="str2">###2###</output></p></div></body></html>'; 

const requestListener = function (req, res) {
    let param1 = "";
    let param2 = "";
    let errormsg = "";
    let contents = "";

    if(req.url.substring(0, 7) === "/parser"){
        const allparam = req.url.split("?");
        param1 = allparam[1].split("&")[0].split("=")[1];
        param2 = allparam[1].split("&")[1].split("=")[1];
        let isValid = param1!="";
        if (!isValid) {
            errormsg = "Укажите Имя !!!";
        } else {
            isValid = isValid && (Number.parseInt(param2) >= 0);
            if (!isValid) {
                errormsg = "Укажите правильный возраст !!!";
            }
        }

        if (isValid) { // страница с правильным результатом
            contents = contentTemplate2.replace("###1###", decodeURI(param1)).replace("###2###", param2);
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200);
            res.end(contents);
        } else { // страница с ошибкой
            contents = contentTemplate1.replace("###1###", decodeURI(param1)).replace("###2###", decodeURI(param2)).replace("###err###", errormsg);
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200);
            res.end(contents);
        }
    }
    else{ // стартовая страница
        param1 = "";
        param2 = "";
        errormsg = "";
        contents = contentTemplate1.replace("###1###", param1).replace("###2###", param2).replace("###err###", errormsg);
        res.setHeader("Content-Type", "text/html");
        res.writeHead(200);
        res.end(contents);
    }
}

const server = http.createServer(requestListener).listen(port);

function logLineSync(logLine) {
    const logDT=new Date();
    let time=logDT.toLocaleDateString()+" "+logDT.toLocaleTimeString();
    let fullLogLine=time+" "+logLine;

    console.log(fullLogLine);
}

