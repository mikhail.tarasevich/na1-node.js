function serializeForm(formNode) {
    return new FormData(formNode);
    /*
        console.log(formNode.elements);
    console.log("-----------------------------");
    console.log(document.getElementById('reqMethodPost').checked);
    console.log(document.getElementById('reqMethodGet').checked);
    console.log(document.getElementById('reqURL').value);
    console.log(document.getElementById('reqHeader').value);
    console.log(document.getElementById('reqBody').value);
    */
}
  
async function handleFormSubmit(event) {
    event.preventDefault();
    const data = serializeForm(event.target);
    console.log(Array.from(data.entries()));
    //const data2json = JSON.stringify(Array.from(data.entries()));
    formData = {};
    //formData["reqMethodPost"] = document.getElementById('reqMethodPost').checked;
    //formData["reqMethodGet"] = document.getElementById('reqMethodGet').checked;
    if (document.getElementById('reqMethodGet').checked) {
        formData["reqMethod"] = "get";
    } else if (document.getElementById('reqMethodPost').checked) {
        formData["reqMethod"] = "post";
    } else {
        formData["reqMethod"] = "";
    }
    
    formData["reqURL"] = document.getElementById('reqURL').value;
    formData["reqHeader"] = document.getElementById('reqHeader').value;
    formData["reqBody"] = document.getElementById('reqBody').value;

    // очистка полей Resault
    document.getElementById('resStatus').value="";
    document.getElementById('resContentType').value="";
    document.getElementById('resHeader').value="";
    document.getElementById('resBody').value="";
    document.getElementById('resPreview').innerHTML="";

    await fetch('http://localhost:4095/postman2', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'  },
      body: JSON.stringify(formData),
     })
    .then(function(response) {
        return response.json();
      })
    .then(function(data) {
        document.getElementById('resStatus').value=data.statusCode;
        document.getElementById('resContentType').value=data.contentType;
        document.getElementById('resHeader').value=reqParser(data.headers);
        document.getElementById('resBody').value=data.data;
        document.getElementById('resPreview').innerHTML=data.data;
    });
}

async function sendData(data) {
    return await fetch('http://localhost:4095/postman2', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'  },
      body: data,
     })
}

function reqParser(obj) {
    var objtext = "";
    for (let k of Object.keys(obj)) {
        console.log("--------------------- object key: " + k );
        if (typeof obj[k] === `object`) {
            console.log(" object ");
            objtext = objtext + "\n" + reqParser(obj[k]);
        } else {
            console.log(" value: " + obj[k] );
            objtext = objtext + "\n" + k + ": " + obj[k]
        }
    };

    return objtext;
}
  
const applicantForm = document.getElementById('postmanForm');
applicantForm.addEventListener('submit', handleFormSubmit);
