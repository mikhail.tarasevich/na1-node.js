const port = 4095;
const express = require('express');
const fs = require('fs');
const path = require('path');
const os = require('os');
const webserver = express();
const fetch = require("isomorphic-fetch");
const mydomain = `http://localhost:4095`;
const axios = require('axios').default;
//const https = require("https");
//const mydomain = `http://188.166.108.27:4095`;

const logFN = path.join(__dirname, '_server.log');
const logPID = path.join(__dirname, '_pid.log');

webserver.use(express.urlencoded({extended:true}));
webserver.use(express.json()); // мидлварь, умеющая обрабатывать тело запроса в формате JSON

webserver.post('/postman1', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"postman1 called");

    logLineSync(logFN,`[${port}] `+ " reqMethod = " + (typeof req.body.reqMethod ));
    for (let k of Object.keys(req.body)) {
        logLineSync(logFN,`[${port}] `+ " (" + k + ") = " + req.body[k] );
        
    }

    res.sendStatus(200);
});

webserver.options('/postman2', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"service2 preflight called");
    res.setHeader("Access-Control-Allow-Origin","*"); // разрешаем запросы с любого origin, вместо * здесь может быть ОДИН origin (протокол+домен+порт)
    res.setHeader("Access-Control-Allow-Headers","Content-Type"); // разрешаем заголовок запроса Content-Type
    res.send(""); 
});

webserver.post('/postman2', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"postman2 called...");
    logLineSync(logFN,`[${port}] `+"postman2 body "+req.body);
    const reqBody = reqParser(req.body);
    //const reqBody = JSON.parse(req.body);
    //const reqBody = reqParser(req[`body`]);
    /*
    for (let k of Object.keys(req[`body`])) {
        logLineSync(logFN,`[${port}] `+ "--------------------- key: " + k );
        if (typeof req.body[k] === `object`) {
            logLineSync(logFN,`[${port}] `+ " object ");
            for (let j of Object.keys(req.body[k])) {
                if (typeof req.body[k][j] === `object`) {
                    logLineSync(logFN,`[${port}] `+ " object2 ");
                } else {
                    logLineSync(logFN,`[${port}] `+ " value2: " + req.body[k][j] );
                }
            }
        } else {
            logLineSync(logFN,`[${port}] `+ " value: " + req.body[k] );
        }
    };
    */

    let AxiosReqConfig = {
        method: reqBody.reqMethod,
        url: reqBody.reqURL,
        //headers: reqBody.reqHeader,
        responseType: 'json'
    };
    logLineSync(logFN,`[${port}] `+"AxiosRequestConfig: "+JSON.stringify(AxiosReqConfig));
    switch (reqBody.reqMethod) {
        case 'get':
            logLineSync(logFN,`[${port}] `+"выбран метод GET");
          break;
        case 'post':
            logLineSync(logFN,`[${port}] `+"выбран метод POST");
            //AxiosReqConfig.data = reqBody.reqBody
          break;
        default:
            logLineSync(logFN,`[${port}] `+"выбран неизвестный метод "+reqBody.reqMethod);
            res.send("Ошибка: выбран неизвестный метод "+reqBody.reqMethod);
      } 

    axios(AxiosReqConfig)
    /*
        axios({
        method: 'get',
        url: 'http://google.com',
        responseType: 'json'
        // `responseType` indicates the type of data that the server will respond with
        // options are: 'arraybuffer', 'document', 'json', 'text', 'stream'
        //   browser only: 'blob'
    })
    */

    .then(function (response) {
        /*
        //logLineSync(logFN,`[${port}] `+ `response.status: `+response.status);
        for (let k of Object.keys(response.data.client[`_httpMessage`])) {
            logLineSync(logFN,`[${port}] `+ "--------------------- key: " + k );
            if (typeof response.data.client[`_httpMessage`][k] === `object`) {
                logLineSync(logFN,`[${port}] `+ " object ");
            } else {
                logLineSync(logFN,`[${port}] `+ " value: " + response.data.client[`_httpMessage`][k] );
            }
        };
*/
        logLineSync(logFN,`[${port}] `+ `axios response.status: `+response.status);

        let resBodyTest = {"111":111};

        let resBody = {};
        resBody[`contentType`] = response.headers[`content-type`];
        resBody[`statusCode`] = response.status;
        resBody[`headers`] = response.headers;
        resBody[`data`] = response.data;
        res.setHeader("Access-Control-Allow-Origin","*");
        res.send(JSON.stringify(resBody));
    
        logLineSync(logFN,`[${port}] `+ `axios send OK`);

      //  res.setHeader("Access-Control-Allow-Origin","*");
       // res.body.status = response.status;
        //res.redirect(301,'postman.html')
      //  res.send("");
    })
    .catch(error => logLineSync(logFN,`[${port}] `+"error axios: "+error));
});

webserver.listen(port,()=>{
    logLineSync(logFN,"web server running on port "+port);

    const filePID = fs.openSync(logPID, 'w+'); 
    fs.writeSync(filePID, process.pid); 
    fs.closeSync(filePID);
});

function logLineSync(logFilePath,logLine) {
    const logDT=new Date();
    let time=logDT.toLocaleDateString()+" "+logDT.toLocaleTimeString();
    let fullLogLine=time+" "+logLine;

    console.log(fullLogLine); // выводим сообщение в консоль

    const logFd = fs.openSync(logFilePath, 'a+'); // и это же сообщение добавляем в лог-файл
    fs.writeSync(logFd, fullLogLine + os.EOL); // os.EOL - это символ конца строки, он разный для разных ОС
    fs.closeSync(logFd);
}

function getCircularReplacer() {
    const seen = new WeakSet();
    return (key, value) => {
      if (typeof value === 'object' && value !== null) {
        if (seen.has(value)) {
          return;
        }
        seen.add(value);
      }
      return value;
    };
};

function reqParser(obj) {
    var body = {};
    for (let k of Object.keys(obj)) {
        logLineSync(logFN,`[${port}] `+ "--------------------- object key: " + k );
        if (typeof obj[k] === `object`) {
            logLineSync(logFN,`[${port}] `+ " object ");
            reqParser(obj[k]);
        } else {
            logLineSync(logFN,`[${port}] `+ " value: " + obj[k] );
            body[k] = obj[k]
        }
    };

    return body;
}