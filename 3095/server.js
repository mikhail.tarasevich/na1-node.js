const port = 3095;
const express = require('express');
const fs = require('fs');
const path = require('path');
const os = require('os');
const webserver = express();
const exphbs = require('express-handlebars');
const fetch = require("isomorphic-fetch");
// const mydomain = `http://localhost:3095`;
const mydomain = `http://188.166.108.27:3095`;

const logFN = path.join(__dirname, '_server.log');
const logPID = path.join(__dirname, '_pid.log');
const fileStat = path.join(__dirname, 'stat.json');
const fileVar = path.join(__dirname, 'variants.json');

webserver.use(express.urlencoded({extended:true}));

webserver.engine('handlebars', exphbs()); // регистрируем движок шаблонов handlebars в списке движков шаблонов express
webserver.set('view engine', 'handlebars'); // устанавливаем, что будет использоваться именно движок шаблонов handlebars
webserver.set('views', path.join(__dirname, 'views')); // задаём папку, в которой будут шаблоны

webserver.get('/variants', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"variants called");

    let variantsJSON=fs.readFileSync(fileVar,"utf8");
    let variants=JSON.parse(variantsJSON);
    res.setHeader("Content-Type", "application/json");
    res.send(variants);
});

webserver.post('/stat', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"stat called");

    let statJSON=fs.readFileSync(fileStat,"utf8");
    let stat=JSON.parse(statJSON);
    res.setHeader("Content-Type", "application/json");
    res.send(stat);
});

webserver.get('/mainpage', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"mainpage called");

    getStat().then((statJSON) => {
        getVariants().then((variantsJSON) => {
            let variants4render={layout:'layout'};
            for (let k of Object.keys(variantsJSON)) {
                variants4render[k] = variantsJSON[k];
                variants4render['b'+k+'Visible'] = true;
                variants4render['vote'+k] = statJSON[k];
            }
            
            res.render('mainpage',variants4render);
        });
    });;
});

webserver.post('/vote', (req, res) => { 
    logLineSync(logFN,`[${port}] `+"vote called");

    getStat().then((statJSON) => {
        statJSON[req.body.variant] = statJSON[req.body.variant] + 1;
        fs.writeFileSync(fileStat,JSON.stringify(statJSON),"utf8");
    });

    res.redirect(301,'/mainpage')
});

async function getVariants() {
    const fetchOptions={
        method: "get",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const resp=await fetch(mydomain + `/variants`, fetchOptions);
    return resp.json();
}

async function getStat() {
    const fetchOptions={
        method: "post",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const resp=await fetch(mydomain + `/stat`, fetchOptions);
    return resp.json();
}

webserver.listen(port,()=>{
    logLineSync(logFN,"web server running on port "+port);

    const filePID = fs.openSync(logPID, 'w+'); 
    fs.writeSync(filePID, process.pid); 
    fs.closeSync(filePID);
});

function logLineSync(logFilePath,logLine) {
    const logDT=new Date();
    let time=logDT.toLocaleDateString()+" "+logDT.toLocaleTimeString();
    let fullLogLine=time+" "+logLine;

    console.log(fullLogLine); // выводим сообщение в консоль

    const logFd = fs.openSync(logFilePath, 'a+'); // и это же сообщение добавляем в лог-файл
    fs.writeSync(logFd, fullLogLine + os.EOL); // os.EOL - это символ конца строки, он разный для разных ОС
    fs.closeSync(logFd);
}

